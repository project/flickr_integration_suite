/**
 * @file
 * This JavaScript file contains a Drupal behavior for initializing and managing
 * a Flickr slider with Swiper plugin. It also includes functionality
 * for displaying images in full screen on click and closing them with various
 * methods.
 */
(function ($, Drupal, once) {
  Drupal.behaviors.flickrSlider = {
    attach(context, settings) {
      $(once('flickrSlider', '.flickr-slider', context)).each(function () {
        const id = $(this).attr('id');

        // Initializes the thumbnail slider using Swiper plugin.
        const swiper = new Swiper(`#${id} .swiper-nav`, {
          lazy: true,
          loop: true,
          spaceBetween: 5,
          slidesPerView: 3,
          freeMode: true,
          watchSlidesProgress: true,
          breakpoints: {
            768: {
              slidesPerView: 3,
            },
            320: {
              slidesPerView: 1,
            },
          },
        });

        // Initializes the main image slider using Swiper plugin.
        new Swiper(`#${id} .swiper-for`, {
          effect: 'slide',
          lazy: true,
          loop: true,
          spaceBetween: 10,
          navigation: {
            nextEl: `#${id} .swiper-button-next`,
            prevEl: `#${id} .swiper-button-prev`,
          },
          thumbs: {
            swiper,
          },
        });

        // Open image in full screen on click
        $('.flickr-slider').on(
          'click',
          '.swiper-for .swiper-slide img',
          function () {
            const imgUrl = $(this).attr('src');
            const imgAlt = $(this).attr('alt');
            const fullScreenImage =
              `<div class="full-screen-image">` +
              `<span class="close-btn">&times;</span>` +
              `<span class="prev-btn">&lt;</span>` +
              `<span class="next-btn">&gt;</span>` +
              `<img src="${imgUrl}" alt="${imgAlt}">` +
              `</div>`;
            $('body').append(fullScreenImage);
            $('.full-screen-image').fadeIn();
          },
        );

        // Close image on clicking close button
        $(document).on('click', '.full-screen-image .close-btn', function () {
          $('.full-screen-image').fadeOut(function () {
            $(this).remove();
          });
        });

        // Close full-screen image on Escape key press
        $(document).on('keydown', function (e) {
          if (e.key === 'Escape') {
            $('.full-screen-image').fadeOut(function () {
              $(this).remove();
            });
          }
        });
      });
    },
  };
})(jQuery, Drupal, once);
