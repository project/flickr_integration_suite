<?php

namespace Drupal\flickr_integration_suite_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\flickr_integration_suite\FlickrIntegrationSuiteApiProvider;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Flickr Galleries block.
 *
 * @Block(
 *   id = "flickr_integration_suite_galleries_block",
 *   admin_label = @Translation("Flickr Galleries"),
 *   category = @Translation("Flickr Integration Suite")
 * )
 */
class FlickrGalleriesBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Flickr Integration Suite Api Provider.
   *
   * @var \Drupal\flickr_integration_suite\FlickrIntegrationSuiteApiProvider
   */
  protected $flickrIntegrationSuiteApiProvider;

  /**
   * Constructs a new FlickrIntegrationSuiteBlock object.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, FlickrIntegrationSuiteApiProvider $flickrIntegrationSuiteApiProvider) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->flickrIntegrationSuiteApiProvider = $flickrIntegrationSuiteApiProvider;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('flickr_integration_suite.api_provider'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'flickr_gallery_id' => '',
      'flickr_per_page' => 50,
      'flickr_page' => 1,
      'flickr_extras' => '',
      'flickr_user_info' => 0,
      'flickr_gallery_info' => 0,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['flickr_integration_suite'] = [
      '#type' => 'details',
      '#title' => $this->t('Flickr Galleries settings'),
      '#open' => TRUE,
      '#tree' => TRUE,
      '#process' => [[static::class, 'formProcessMergeParent']],
    ];

    $form['flickr_integration_suite']['flickr_gallery_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Gallery ID'),
      '#description' => $this->t('The ID of the gallery of photos to return.'),
      '#default_value' => $this->configuration['flickr_gallery_id'],
      '#required' => TRUE,
      '#maxlength' => 64,
      '#size' => 64,
    ];

    $form['flickr_integration_suite']['flickr_per_page'] = [
      '#type' => 'number',
      '#title' => $this->t('Number of photos'),
      '#description' => $this->t('Number of photos to return per page. The maximum allowed value is 500.'),
      '#default_value' => $this->configuration['flickr_per_page'],
      '#min' => 1,
      '#max' => 500,
    ];

    $form['flickr_integration_suite']['flickr_page'] = [
      '#type' => 'number',
      '#title' => $this->t('Page'),
      '#description' => $this->t('The page of results to return.'),
      '#default_value' => $this->configuration['flickr_page'],
      '#min' => 1,
    ];

    $form['flickr_integration_suite']['flickr_extras'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Extras'),
      '#description' => $this->t('A comma-delimited list of extra information to fetch for each returned record. Currently supported fields are: description, license, date_upload, date_taken, owner_name, icon_server, original_format, last_update, geo, tags, machine_tags, o_dims, views, media, path_alias, url_sq, url_t, url_s, url_q, url_m, url_n, url_z, url_c, url_l, url_o'),
      '#default_value' => $this->configuration['flickr_extras'],
    ];

    $form['flickr_integration_suite']['flickr_user_info'] = [
      '#type' => 'radios',
      '#title' => $this->t('Get user info'),
      '#description' => $this->t('Fetch user details'),
      '#default_value' => $this->configuration['flickr_user_info'],
      '#options' => [
        0 => $this->t('No'),
        1 => $this->t('Yes'),
      ],
    ];

    $form['flickr_integration_suite']['flickr_gallery_info'] = [
      '#type' => 'radios',
      '#title' => $this->t('Get gallery info'),
      '#description' => $this->t('Fetch gallery info'),
      '#default_value' => $this->configuration['flickr_gallery_info'],
      '#options' => [
        0 => $this->t('No'),
        1 => $this->t('Yes'),
      ],
    ];

    return $form;
  }

  /**
   * Render API callback that moves elements up a level.
   */
  public static function formProcessMergeParent($element) {
    $parents = $element['#parents'];
    array_pop($parents);
    $element['#parents'] = $parents;
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['flickr_gallery_id'] = $form_state->getValue('flickr_gallery_id');
    $this->configuration['flickr_per_page'] = $form_state->getValue('flickr_per_page');
    $this->configuration['flickr_page'] = $form_state->getValue('flickr_page');
    $this->configuration['flickr_extras'] = $form_state->getValue('flickr_extras');
    $this->configuration['flickr_user_info'] = $form_state->getValue('flickr_user_info');
    $this->configuration['flickr_gallery_info'] = $form_state->getValue('flickr_gallery_info');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $gallery_id = $this->configuration['flickr_gallery_id'];
    $per_page = $this->configuration['flickr_per_page'];
    $page = $this->configuration['flickr_page'];
    $extras = $this->configuration['flickr_extras'];
    $get_user_info = $this->configuration['flickr_user_info'];
    $get_gallery_info = $this->configuration['flickr_gallery_info'];
    $data = $this->flickrIntegrationSuiteApiProvider->galleriesGetPhotos($gallery_id, $per_page, $page, $extras, $get_user_info, $get_gallery_info);

    return [
      '#theme' => 'flickr_slider',
      '#data' => $data,
    ];
  }

}
