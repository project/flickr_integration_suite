# Flickr Integration Suite Block

The Flickr Integration Suite Block module provides a block for displaying Flickr
photos on your drupal site.

## Requirements

- [Flickr Integration Suite](https://www.drupal.org/project/flickr_integration_suite)

## Installation

### Using the Drupal User Interface (easy):

1. Navigate to the 'Extend' page (admin/modules) via the manage administrative
menu.
2. Locate the Flickr Integration Suite Block module and select the checkbox next
to it.
3. Click on 'Install' to enable the Flickr Integration Suite Block module.

### Or use the command line (advanced, but very efficient).

- To install a module with Drush, execute the command below: <br>
`drush en flickr_integration_suite_block`

## Configuration

1. Navigate to the block layout configuration page (`admin/structure/block`).
2. Add the "Flickr Photosets" or "Flickr Galleries" block to the desired region.
3. Configure the Flickr block settings.
