<?php

namespace Drupal\flickr_integration_suite_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\flickr_integration_suite\FlickrIntegrationSuiteApiProvider;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'Flickr Galleries Slider' formatter.
 *
 * @FieldFormatter(
 *   id = "flickr_galleries_slider",
 *   label = @Translation("Flickr Galleries Slider"),
 *   field_types = {
 *     "flickr_galleries"
 *   }
 * )
 */
class FlickrGalleriesSliderFormatter extends FormatterBase {

  /**
   * Flickr Integration Suite Api Provider.
   *
   * @var \Drupal\flickr_integration_suite\FlickrIntegrationSuiteApiProvider
   */
  protected $flickrIntegrationSuiteApiProvider;

  /**
   * Constructs a new FlickrGalleriesSliderFormatter object.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, FlickrIntegrationSuiteApiProvider $flickrIntegrationSuiteApiProvider) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->flickrIntegrationSuiteApiProvider = $flickrIntegrationSuiteApiProvider;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('flickr_integration_suite.api_provider')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    $per_page = $this->getFieldSetting('flickr_per_page');
    $page = $this->getFieldSetting('flickr_page');
    $extras = $this->getFieldSetting('flickr_extras');
    $get_user_info = $this->getFieldSetting('flickr_user_info');
    $get_gallery_info = $this->getFieldSetting('flickr_gallery_info');

    foreach ($items as $delta => $item) {
      $gallery_id = $item->value;
      $data = $this->flickrIntegrationSuiteApiProvider->galleriesGetPhotos($gallery_id, $per_page, $page, $extras, $get_user_info, $get_gallery_info);

      $element[$delta] = [
        '#theme' => 'flickr_slider',
        '#data' => $data,
      ];
    }

    return $element;
  }

}
