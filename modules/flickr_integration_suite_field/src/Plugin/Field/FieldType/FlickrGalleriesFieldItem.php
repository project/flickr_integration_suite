<?php

namespace Drupal\flickr_integration_suite_field\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Defines the Flickr Galleries field type.
 *
 * @FieldType(
 *   id = "flickr_galleries",
 *   label = @Translation("Flickr Galleries"),
 *   description = @Translation("Field to integrate Flickr Galleries."),
 *   category = "flickr_integration_suite_field",
 *   default_widget = "flickr_galleries_default",
 *   default_formatter = "flickr_galleries_slider"
 * )
 */
class FlickrGalleriesFieldItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return [
      'flickr_per_page' => '50',
      'flickr_page' => '1',
      'flickr_extras' => '',
      'flickr_user_info' => 0,
      'flickr_gallery_info' => 0,
    ] + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $element = [];

    $element['flickr_integration_suite'] = [
      '#type' => 'details',
      '#title' => $this->t('Flickr Galleries settings'),
      '#open' => TRUE,
      '#tree' => TRUE,
      '#process' => [[static::class, 'formProcessMergeParent']],
    ];

    $element['flickr_integration_suite']['flickr_per_page'] = [
      '#type' => 'number',
      '#title' => $this->t('Number of photos'),
      '#description' => $this->t('Number of photos to return per page. The maximum allowed value is 500.'),
      '#default_value' => $this->getSetting('flickr_per_page'),
      '#min' => 1,
      '#max' => 500,
    ];

    $element['flickr_integration_suite']['flickr_page'] = [
      '#type' => 'number',
      '#title' => $this->t('Page'),
      '#description' => $this->t('The page of results to return.'),
      '#default_value' => $this->getSetting('flickr_page'),
      '#min' => 1,
    ];

    $element['flickr_integration_suite']['flickr_extras'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Extras'),
      '#description' => $this->t('A comma-delimited list of extra information to fetch for each returned record. Currently supported fields are: description, license, date_upload, date_taken, owner_name, icon_server, original_format, last_update, geo, tags, machine_tags, o_dims, views, media, path_alias, url_sq, url_t, url_s, url_q, url_m, url_n, url_z, url_c, url_l, url_o'),
      '#default_value' => $this->getSetting('flickr_extras'),
    ];

    $element['flickr_integration_suite']['flickr_user_info'] = [
      '#type' => 'radios',
      '#title' => $this->t('Get user info'),
      '#description' => $this->t('Fetch user details'),
      '#default_value' => $this->getSetting('flickr_user_info'),
      '#options' => [
        0 => $this->t('No'),
        1 => $this->t('Yes'),
      ],
    ];

    $element['flickr_integration_suite']['flickr_gallery_info'] = [
      '#type' => 'radios',
      '#title' => $this->t('Get gallery info'),
      '#description' => $this->t('Fetch gallery info'),
      '#default_value' => $this->getSetting('flickr_gallery_info'),
      '#options' => [
        0 => $this->t('No'),
        1 => $this->t('Yes'),
      ],
    ];

    return $element;
  }

  /**
   * Render API callback that moves elements up a level.
   */
  public static function formProcessMergeParent($element) {
    $parents = $element['#parents'];
    array_pop($parents);
    $element['#parents'] = $parents;
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('value')->getValue();
    return $value === NULL || $value === '';
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['value'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Gallery ID'))
      ->setDescription(new TranslatableMarkup('The ID of the gallery of photos to return.'))
      ->setRequired(TRUE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'value' => [
          'description' => 'The ID of the gallery of photos to return.',
          'type' => 'varchar',
          'length' => 255,
        ],
      ],
    ];
  }

}
