<?php

namespace Drupal\flickr_integration_suite_field\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Defines the Flickr Photosets field type.
 *
 * @FieldType(
 *   id = "flickr_photosets",
 *   label = @Translation("Flickr Photosets"),
 *   description = @Translation("Field to integrate Flickr Photosets."),
 *   category = "flickr_integration_suite_field",
 *   default_widget = "flickr_photosets_default",
 *   default_formatter = "flickr_photosets_slider"
 * )
 */
class FlickrPhotosetsFieldItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return [
      'flickr_per_page' => '50',
      'flickr_page' => '1',
      'flickr_media' => 'all',
      'flickr_extras' => '',
    ] + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $element = [];

    $element['flickr_integration_suite'] = [
      '#type' => 'details',
      '#title' => $this->t('Flickr Photosets settings'),
      '#open' => TRUE,
      '#tree' => TRUE,
      '#process' => [[static::class, 'formProcessMergeParent']],
    ];

    $element['flickr_integration_suite']['flickr_per_page'] = [
      '#type' => 'number',
      '#title' => $this->t('Number of photos'),
      '#description' => $this->t('Number of photos to return per page. The maximum allowed value is 500.'),
      '#default_value' => $this->getSetting('flickr_per_page'),
      '#min' => 1,
      '#max' => 500,
    ];

    $element['flickr_integration_suite']['flickr_page'] = [
      '#type' => 'number',
      '#title' => $this->t('Page'),
      '#description' => $this->t('The page of results to return.'),
      '#default_value' => $this->getSetting('flickr_page'),
      '#min' => 1,
    ];

    $element['flickr_integration_suite']['flickr_media'] = [
      '#type' => 'select',
      '#title' => $this->t('Media type'),
      '#options' => [
        'all' => $this->t('All'),
        'photos' => $this->t('Photos'),
        'videos' => $this->t('Videos'),
      ],
      '#description' => $this->t('Filter results by media type.'),
      '#default_value' => $this->getSetting('flickr_media'),
    ];

    $element['flickr_integration_suite']['flickr_extras'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Extras'),
      '#description' => $this->t('A comma-delimited list of extra information to fetch for each returned record. Currently supported fields are: license, date_upload, date_taken, owner_name, icon_server, original_format, last_update, geo, tags, machine_tags, o_dims, views, media, path_alias, url_sq, url_t, url_s, url_m, url_o'),
      '#default_value' => $this->getSetting('flickr_extras'),
    ];

    return $element;
  }

  /**
   * Render API callback that moves elements up a level.
   */
  public static function formProcessMergeParent($element) {
    $parents = $element['#parents'];
    array_pop($parents);
    $element['#parents'] = $parents;
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('value')->getValue();
    return $value === NULL || $value === '';
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['value'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Photoset ID'))
      ->setDescription(new TranslatableMarkup('The id of the photoset to return the photos for.'))
      ->setRequired(TRUE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'value' => [
          'description' => 'The id of the photoset to return the photos for.',
          'type' => 'varchar',
          'length' => 255,
        ],
      ],
    ];
  }

}
