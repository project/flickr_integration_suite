<?php

namespace Drupal\flickr_integration_suite_field\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Defines the Flickr Photosets field widget.
 *
 * @FieldWidget(
 *   id = "flickr_photosets_default",
 *   label = @Translation("Flickr Photosets Widget"),
 *   field_types = {
 *     "flickr_photosets"
 *   }
 * )
 */
class FlickrPhotosetsWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element['value'] = $element + [
      '#type' => 'textfield',
      '#default_value' => $items[$delta]->value ?? NULL,
      '#size' => 60,
      '#attributes' => ['class' => ['js-text-full', 'text-full']],
      '#suffix' => new TranslatableMarkup('The ID of the flickr photoset to return the photos for.'),
    ];

    return $element;
  }

}
