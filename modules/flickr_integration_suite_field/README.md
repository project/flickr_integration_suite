# Flickr Integration Suite Field

The Flickr Integration Suite Field module provides a field type for integrating 
Flickr photos into content entities on your drupal site.

## Requirements

- [Flickr Integration Suite](https://www.drupal.org/project/flickr_integration_suite)

## Installation

### Using the Drupal User Interface (easy):

1. Navigate to the 'Extend' page (admin/modules) via the manage administrative 
menu.
2. Locate the Flickr Integration Suite Field module and select the checkbox next 
to it.
3. Click on 'Install' to enable the Flickr Integration Suite Field module.

### Or use the command line (advanced, but very efficient).

- To install a module with Drush, execute the command below: <br>
`drush en flickr_integration_suite_field`

## Configuration

1. Navigate to the content type listing page (`admin/structure/types`).
2. Edit the desired content type where you want to add the Flickr field.
3. Add a new field of type "Flickr Photosets" or "Flickr Galleries" to the 
content type.
4. Configure the Flickr field settings.
5. Next, add the field to your content type's form display and manage display 
settings as needed.
6. To associate photos with the field, add the ID of the desired Photoset or 
Gallery in the field settings while adding the content.
