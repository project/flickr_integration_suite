<?php

namespace Drupal\flickr_integration_suite;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Http\ClientFactory;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\key\KeyRepositoryInterface;

/**
 * Flickr API Service provider.
 */
class FlickrIntegrationSuiteApiProvider {

  use StringTranslationTrait;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The key repository interface.
   *
   * @var \Drupal\key\KeyRepositoryInterface
   */
  protected $keyRepository;

  /**
   * The http_client_factory service.
   *
   * @var \Drupal\Core\Http\ClientFactory
   */
  protected $httpClientFactory;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  private $cacheBackend;

  /**
   * The Flickr config.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $flickrConfig;

  /**
   * The Flickr Api Endpoint.
   */
  protected string $flickrApiEndpoint;

  /**
   * The Flickr Api Key.
   */
  protected string $flickrApiKey;

  /**
   * The Flickr Api Cache max-age.
   */
  protected string $flickrApiCacheMaxAge;

  /**
   * Constructs a new FlickrIntegrationSuiteApiProvider object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\key\KeyRepositoryInterface $key_repository
   *   The Key Repository interface.
   * @param \Drupal\Core\Http\ClientFactory $http_client_factory
   *   The http_client_factory service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cacheBackend
   *   Cache backend.
   */
  public function __construct(ConfigFactoryInterface $config_factory, KeyRepositoryInterface $key_repository, ClientFactory $http_client_factory, MessengerInterface $messenger, CacheBackendInterface $cacheBackend) {
    $this->configFactory = $config_factory;
    $this->keyRepository = $key_repository;
    $this->httpClientFactory = $http_client_factory;
    $this->messenger = $messenger;
    $this->cacheBackend = $cacheBackend;

    $this->flickrConfig = $this->configFactory->get('flickr_integration_suite.settings');
    $this->flickrApiEndpoint = $this->flickrConfig->get('api_endpoint');
    $this->flickrApiKey = $this->flickrConfig->get('api_key') ? $this->keyRepository->getKey($this->flickrConfig->get('api_key'))->getKeyValue() : '';
    $this->flickrApiCacheMaxAge = $this->flickrConfig->get('api_cache_max_age');
  }

  /**
   * Generate Hash from parameters array.
   *
   * @param array $parameters
   *   Request parameters.
   *
   * @return string
   *   Return Hash string.
   */
  private function generateParamHash(array $parameters) {
    ksort($parameters);
    $paramHash = '';

    foreach ($parameters as $k => $v) {
      $paramHash .= $k . $v;
    }

    return $paramHash;
  }

  /**
   * Flickr request.
   *
   * Sends a request to the Flickr API with the given parameters.
   *
   * @param array $parameters
   *   The parameters to be sent with the request.
   *
   * @return bool|mixed
   *   The response data or FALSE on failure.
   */
  public function request($parameters) {
    if (!$this->flickrApiKey) {
      $this->messenger->addError($this->t('Flickr API credentials are not set. It can be set on the <a href=":config_page">configuration page</a>.', [
        ':config_page' => Url::fromRoute('flickr_integration_suite.settings_form')->toString(),
      ]));
      return FALSE;
    }

    // Include extra information 'media' to identify media type.
    if (isset($parameters['extras']) && !strpos($parameters['extras'], "media")) {
      $parameters['extras'] .= ',media';
    }

    $paramHash = $this->generateParamHash($parameters);
    $cid = 'flickr_integration_suite:' . md5($paramHash);

    if ($cache = $this->cacheBackend->get($cid)) {
      // Return result from cache if found.
      return $cache->data;
    }
    else {
      $client = $this->httpClientFactory->fromOptions();
      $response = $client->get($this->flickrApiEndpoint, ['query' => $parameters]);

      if ($response) {
        $data = json_decode((string) $response->getBody(), TRUE);

        if (isset($data['stat']) && $data['stat'] == 'fail') {
          if (!empty($data['message'])) {
            $this->messenger->addError($this->t('Flickr API :method Error - :message', [
              ':method' => $parameters['method'],
              ':message' => $data['message'],
            ]));
          }
        }

        // Cache the response.
        if ($this->flickrApiCacheMaxAge != 0) {
          $this->cacheBackend->set($cid, $data, time() + $this->flickrApiCacheMaxAge);
        }

        return $data;
      }
    }

    return FALSE;
  }

  /**
   * The flickr.photosets.getPhotos service.
   *
   * This method retrieves the list of photos and videos in a set.
   *
   * @param string $photoset_id
   *   The ID of the photoset to retrieve photos from.
   * @param int $per_page
   *   The number of photos to return per page. Default is 50.
   * @param int $page
   *   The page of results to return. Default is 1.
   * @param string $media
   *   Filter results by media type. Valid values are 'all' (default), 'photos',
   *   or 'videos'.
   * @param string $extras
   *   A comma-delimited list of extra information to fetch for each returned
   *   record.
   *
   * @return array
   *   An array containing the photos and videos in the photoset.
   *
   * @see https://www.flickr.com/services/api/flickr.photosets.getPhotos.html
   */
  public function photosetsGetPhotos($photoset_id, $per_page = 50, $page = 1, $media = 'all', $extras = '') {
    $parameters = [
      'method' => 'flickr.photosets.getPhotos',
      'api_key' => $this->flickrApiKey,
      'photoset_id' => $photoset_id,
      'format' => 'json',
      'nojsoncallback' => 1,
      'per_page' => $per_page,
      'page' => $page,
      'extras' => $extras,
      'media' => $media,
    ];

    return $this->request($parameters);
  }

  /**
   * The flickr.galleries.getPhotos service.
   *
   * This method returns the list of photos and videos for a gallery.
   *
   * @param string $gallery_id
   *   The ID of the gallery to retrieve photos from.
   * @param int $per_page
   *   The number of photos to return per page. Default is 50.
   * @param int $page
   *   The page of results to return. Default is 1.
   * @param string $extras
   *   A comma-delimited list of extra information to fetch for each returned
   *   record.
   * @param int $get_user_info
   *   A flag to indicate whether to return user information. Default is 0.
   * @param int $get_gallery_info
   *   A flag to indicate whether to return gallery information. Default is 0.
   *
   * @return array
   *   An array containing the photos in the gallery.
   *
   * @see https://www.flickr.com/services/api/flickr.galleries.getPhotos.html
   */
  public function galleriesGetPhotos(string $gallery_id, int $per_page = 50, int $page = 1, string $extras = '', int $get_user_info = 0, int $get_gallery_info = 0): array {
    $parameters = [
      'method' => 'flickr.galleries.getPhotos',
      'api_key' => $this->flickrApiKey,
      'gallery_id' => $gallery_id,
      'format' => 'json',
      'nojsoncallback' => 1,
      'per_page' => $per_page,
      'page' => $page,
      'extras' => $extras,
      'get_user_info' => $get_user_info,
      'get_gallery_info' => $get_gallery_info,
    ];
    return $this->request($parameters);
  }

  /**
   * The flickr.photos.getSizes service.
   *
   * This method retrieves the available sizes for a photo.
   * The calling user must have permission to view the photo.
   *
   * @param string $photo_id
   *   The ID of the photo to retrieve sizes for.
   *
   * @return array
   *   An array containing the available sizes for the photo.
   *
   * @see https://www.flickr.com/services/api/flickr.photos.getSizes.html
   */
  public function photosGetSizes($photo_id) {
    $parameters = [
      'method' => 'flickr.photos.getSizes',
      'api_key' => $this->flickrApiKey,
      'photo_id' => $photo_id,
      'format' => 'json',
      'nojsoncallback' => 1,
    ];

    return $this->request($parameters);
  }

}
