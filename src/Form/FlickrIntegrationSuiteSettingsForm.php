<?php

namespace Drupal\flickr_integration_suite\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Flickr integration suite settings for this site.
 */
class FlickrIntegrationSuiteSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'flickr_integration_suite_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['flickr_integration_suite.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['api_endpoint'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Flickr API Endpoint'),
      '#required' => TRUE,
      '#default_value' => $this->config('flickr_integration_suite.settings')->get('api_endpoint'),
    ];

    $form['api_key'] = [
      '#type' => 'key_select',
      '#title' => $this->t('Flickr API Key'),
      '#required' => TRUE,
      '#default_value' => $this->config('flickr_integration_suite.settings')->get('api_key'),
    ];

    $period = [
      0 => '<' . $this->t('no caching') . '>',
      60 => $this->t('1 minute'),
      180 => $this->t('3 minutes'),
      300 => $this->t('5 minutes'),
      600 => $this->t('10 minutes'),
      900 => $this->t('15 minutes'),
      1800 => $this->t('30 minutes'),
      2700 => $this->t('45 minutes'),
      3600 => $this->t('1 hour'),
      10800 => $this->t('3 hours'),
      21600 => $this->t('6 hours'),
      32400 => $this->t('9 hours'),
      43200 => $this->t('12 hours'),
      86400 => $this->t('1 day'),
    ];

    $form['api_cache_max_age'] = [
      '#type' => 'select',
      '#title' => $this->t('Flickr API cache max age'),
      '#default_value' => $this->config('flickr_integration_suite.settings')->get('api_cache_max_age'),
      '#options' => $period,
      '#description' => $this->t('The maximum time an API request can be cached by Drupal.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Save the configured API endpoint and API key.
    $this->config('flickr_integration_suite.settings')
      ->set('api_endpoint', $form_state->getValue('api_endpoint'))
      ->set('api_key', $form_state->getValue('api_key'))
      ->set('api_cache_max_age', $form_state->getValue('api_cache_max_age'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
