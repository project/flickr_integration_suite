# Flickr Integration Suite

- The Flickr Integration Suite module provides integration with the Flickr
platform for drupal websites.
- It allows users to easily display Flickr photos on their drupal site,
providing various functionalities to enhance the user experience.

## Features

- Seamless integration with Flickr API.
- Display Flickr photos on drupal content.
- Configurable options for displaying Flickr photos.
- Extendable architecture for further customization.

## Requirements

1. Flickr API application key. [See here](https://www.flickr.com/services/apps/create/) for more details.
2. Contributed module - [Key](https://www.drupal.org/project/key)

## Installation

### Using the Drupal User Interface (easy):

1. Navigate to the 'Extend' page (admin/modules) via the manage administrative
menu.
2. Locate the Flickr Integration Suite module and select the checkbox next to
it.
3. Click on 'Install' to enable the Flickr Integration Suite module.

### Or use the command line (advanced, but very efficient).

- To enable Flickr Integration Suite module with Drush, execute the command
below: <br> `drush en flickr_integration_suite`
